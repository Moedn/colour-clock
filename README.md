# Moe's Colour Clock

This clock displays the hour as a defined spectral colour (see blow [How to Read the Clock](#how-to-read-the-clock).
I built this, because I get stressed by the knowledge of the exact hour

	"Oh, it's 13.24h, that's 6min to 1.30pm, I still haven't eaten, but on 15h I'll have that meeting and still need to do […]")

…so you won't find any clocks or watches in my house.
However, moving through the day I still need a rough estimate about how late it actually is.
Of course, the position of the sun is a good start for that, but since Germany is quite cloudy and the approach naturally doesn't work at night, the colour clock is a good solution for me.

With a little training, I was able to identify ¼h segments by their hue.

More details are given in the [repository of the code of this clock](https://gitlab.com/Moedn/colour-clock-code/).

---

Here's a implementation example:

![colour-clock-in-a-cup](images/colour-clock-in-a-cup.jpg){ width=60% }

# How to Read the Clock

256 colours get mapped onto a 12h segment of your day:

![hour-as-spectra-colour](https://gitlab.com/Moedn/colour-clock-code/-/raw/v1.0.0/images/hour-as-spectra-colour.png){ width=100% }

# Build It

## Compenents Used

- ESP32 (I used DEV KIT V1, see [Connections](#connections))
- LED Ring (8Bit RGB WS2812 5V similar to Neopixel Arduino Raspberry Pi ESP)
	+ just adjust the number to LEDs in the code if you choose a different variant

## Instructions

1. prepare microcontroller connections
	- solder 5V connection (see [Connections](#connections) below
	- plug cables on pins (D4, GND)
2. solder LED ring connections
	- 5V → Power 5V DC
	- GND → Power Signal Ground
	- D4 → Data Input
3. flash microcontroller via USB (using Arduino IDE)
	- Board: DOIT ESP32 DEVKIT V1
	- Upload Speed: 921600
	- libraries
		+ FastLED
		+ Adafruit NeoPixel (seems to be optional)
	- code: [colour-clock-code v1.0.0](https://gitlab.com/Moedn/colour-clock-code/-/releases#v1.0.0)
4. enjoy your existence

### Connections

If your ESP32 variant doesn't come with a 5V pin, you can 'steal' a +5V supply from the voltage transformer:

![connections](images/connections.jpg){ width=60% }